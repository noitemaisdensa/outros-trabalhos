﻿using Nsf._2018.ProjetoIntegrador.PuroTempero.User_Control;
using Nsf._2018.ProjetoIntegrador.PuroTempero.User_Control.Cliente;
using Nsf._2018.ProjetoIntegrador.PuroTempero.User_Control.Funcionário;
using Nsf._2018.ProjetoIntegrador.PuroTempero.User_Control.Produtos;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Nsf._2018.ProjetoIntegrador.PuroTempero.Forms
{
    public partial class frmMenu : Form
    {
        public frmMenu()
        {
            InitializeComponent();
        }
        public void OpenScreen(UserControl control)
        {
            // Se a quantidades de CONTROLES for igual a 1, Ou seja, se já tiver uma tela dentro da GROUP BOX.
            if (pnlConteudo.Controls.Count == 1)
                pnlConteudo.Controls.RemoveAt(0);
                pnlConteudo.Controls.Add(control);
            //Eu irei REMOVER o primeiro ÍNDICE do CONTROLE
            //Ou seja, sempre quando eu for abrir uma tela nova, ele vai remover a tela ATUAL e ADICIONA a NOVA.
        }

        private void lblFechar_Click(object sender, EventArgs e)
        {
            DialogResult dialog = MessageBox.Show("Tem certeza que deseja fechar o Programa",
                                                 "ATENÇÃO",
                                                 MessageBoxButtons.YesNo,
                                                 MessageBoxIcon.Exclamation);

            if (dialog == DialogResult.Yes)
            {
                Application.Exit();
            }
        }

        private void cadastrarFuncionárioToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmSalvar_Funcionario tela = new frmSalvar_Funcionario();
            OpenScreen(tela);
        }

        private void alterarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmAlterar_Funcionario tela = new frmAlterar_Funcionario();
            OpenScreen(tela);
        }

        private void consultarFuncionárioToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmConsultar_Funcionario tela = new frmConsultar_Funcionario();
            OpenScreen(tela);
        }

        private void cadastrarClienteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmCadastrar_Cliente tela = new frmCadastrar_Cliente();
            OpenScreen(tela);
        }

        private void consultarClienteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmConsultar_Cliente tela = new frmConsultar_Cliente();
            OpenScreen(tela);
        }

        private void cadastrarProdutoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmCadastrar_Produtos tela = new frmCadastrar_Produtos();
            OpenScreen(tela);
        }
    }
}
