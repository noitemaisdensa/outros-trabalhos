﻿namespace Nsf._2018.ProjetoIntegrador.PuroTempero.User_Control
{
    partial class frmAlterar_Funcionario
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.nudConvenio = new System.Windows.Forms.NumericUpDown();
            this.nudVA = new System.Windows.Forms.NumericUpDown();
            this.lblConvenio = new System.Windows.Forms.Label();
            this.nudVT = new System.Windows.Forms.NumericUpDown();
            this.cboCidade = new System.Windows.Forms.ComboBox();
            this.lblCidade = new System.Windows.Forms.Label();
            this.nudNumero = new System.Windows.Forms.NumericUpDown();
            this.cboUF = new System.Windows.Forms.ComboBox();
            this.txtCEP = new System.Windows.Forms.MaskedTextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.lblUF = new System.Windows.Forms.Label();
            this.txtComplemento = new System.Windows.Forms.TextBox();
            this.nudVR = new System.Windows.Forms.NumericUpDown();
            this.lblVR = new System.Windows.Forms.Label();
            this.lblVA = new System.Windows.Forms.Label();
            this.lblVT = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.gpbAcesso = new System.Windows.Forms.GroupBox();
            this.chkVendas = new System.Windows.Forms.CheckBox();
            this.txtSenha = new System.Windows.Forms.TextBox();
            this.txtUsuario = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.lblUsuario = new System.Windows.Forms.Label();
            this.chkFinanceiro = new System.Windows.Forms.CheckBox();
            this.chkCompras = new System.Windows.Forms.CheckBox();
            this.chkRH = new System.Windows.Forms.CheckBox();
            this.chkFuncionario = new System.Windows.Forms.CheckBox();
            this.chkADM = new System.Windows.Forms.CheckBox();
            this.nudSalario = new System.Windows.Forms.NumericUpDown();
            this.button2 = new System.Windows.Forms.Button();
            this.lblCompemento = new System.Windows.Forms.Label();
            this.gpbDadosPessoais = new System.Windows.Forms.GroupBox();
            this.txtTelefone = new System.Windows.Forms.MaskedTextBox();
            this.lblTelefone = new System.Windows.Forms.Label();
            this.btnProcurar = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.dtpNascimento = new System.Windows.Forms.DateTimePicker();
            this.txtRG = new System.Windows.Forms.MaskedTextBox();
            this.chkM = new System.Windows.Forms.RadioButton();
            this.chkF = new System.Windows.Forms.RadioButton();
            this.txtCPF = new System.Windows.Forms.MaskedTextBox();
            this.txtNome = new System.Windows.Forms.TextBox();
            this.lblSexo = new System.Windows.Forms.Label();
            this.lblDataNascimento = new System.Windows.Forms.Label();
            this.lblRG = new System.Windows.Forms.Label();
            this.lblCPF = new System.Windows.Forms.Label();
            this.lblNome = new System.Windows.Forms.Label();
            this.btnAlterar = new System.Windows.Forms.Button();
            this.gpbEndereco = new System.Windows.Forms.GroupBox();
            this.imagePesquisa = new System.Windows.Forms.PictureBox();
            this.lblEndereco = new System.Windows.Forms.Label();
            this.txtEndereco = new System.Windows.Forms.TextBox();
            this.lblCEP = new System.Windows.Forms.Label();
            this.gpbSalario = new System.Windows.Forms.GroupBox();
            ((System.ComponentModel.ISupportInitialize)(this.nudConvenio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudVA)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudVT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudNumero)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudVR)).BeginInit();
            this.gpbAcesso.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudSalario)).BeginInit();
            this.gpbDadosPessoais.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.gpbEndereco.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imagePesquisa)).BeginInit();
            this.gpbSalario.SuspendLayout();
            this.SuspendLayout();
            // 
            // nudConvenio
            // 
            this.nudConvenio.Cursor = System.Windows.Forms.Cursors.Hand;
            this.nudConvenio.DecimalPlaces = 2;
            this.nudConvenio.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.nudConvenio.ForeColor = System.Drawing.Color.Black;
            this.nudConvenio.Increment = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.nudConvenio.Location = new System.Drawing.Point(248, 90);
            this.nudConvenio.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.nudConvenio.Name = "nudConvenio";
            this.nudConvenio.Size = new System.Drawing.Size(140, 20);
            this.nudConvenio.TabIndex = 40;
            this.nudConvenio.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // nudVA
            // 
            this.nudVA.Cursor = System.Windows.Forms.Cursors.Hand;
            this.nudVA.DecimalPlaces = 2;
            this.nudVA.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.nudVA.ForeColor = System.Drawing.Color.Black;
            this.nudVA.Increment = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.nudVA.Location = new System.Drawing.Point(35, 90);
            this.nudVA.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.nudVA.Name = "nudVA";
            this.nudVA.Size = new System.Drawing.Size(140, 20);
            this.nudVA.TabIndex = 40;
            this.nudVA.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblConvenio
            // 
            this.lblConvenio.AutoSize = true;
            this.lblConvenio.Font = new System.Drawing.Font("Segoe UI", 8F, System.Drawing.FontStyle.Bold);
            this.lblConvenio.ForeColor = System.Drawing.Color.Black;
            this.lblConvenio.Location = new System.Drawing.Point(190, 92);
            this.lblConvenio.Name = "lblConvenio";
            this.lblConvenio.Size = new System.Drawing.Size(60, 13);
            this.lblConvenio.TabIndex = 39;
            this.lblConvenio.Text = "Convênio:";
            // 
            // nudVT
            // 
            this.nudVT.Cursor = System.Windows.Forms.Cursors.Hand;
            this.nudVT.DecimalPlaces = 2;
            this.nudVT.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.nudVT.ForeColor = System.Drawing.Color.Black;
            this.nudVT.Increment = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.nudVT.Location = new System.Drawing.Point(37, 56);
            this.nudVT.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.nudVT.Name = "nudVT";
            this.nudVT.Size = new System.Drawing.Size(140, 20);
            this.nudVT.TabIndex = 40;
            this.nudVT.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // cboCidade
            // 
            this.cboCidade.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cboCidade.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboCidade.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.cboCidade.ForeColor = System.Drawing.Color.Black;
            this.cboCidade.FormattingEnabled = true;
            this.cboCidade.Location = new System.Drawing.Point(209, 108);
            this.cboCidade.Name = "cboCidade";
            this.cboCidade.Size = new System.Drawing.Size(128, 21);
            this.cboCidade.TabIndex = 40;
            // 
            // lblCidade
            // 
            this.lblCidade.AutoSize = true;
            this.lblCidade.Font = new System.Drawing.Font("Segoe UI", 8F, System.Drawing.FontStyle.Bold);
            this.lblCidade.ForeColor = System.Drawing.Color.Black;
            this.lblCidade.Location = new System.Drawing.Point(166, 111);
            this.lblCidade.Name = "lblCidade";
            this.lblCidade.Size = new System.Drawing.Size(46, 13);
            this.lblCidade.TabIndex = 41;
            this.lblCidade.Text = "Cidade:";
            // 
            // nudNumero
            // 
            this.nudNumero.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.nudNumero.ForeColor = System.Drawing.Color.Black;
            this.nudNumero.Location = new System.Drawing.Point(197, 81);
            this.nudNumero.Name = "nudNumero";
            this.nudNumero.Size = new System.Drawing.Size(140, 20);
            this.nudNumero.TabIndex = 38;
            this.nudNumero.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // cboUF
            // 
            this.cboUF.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cboUF.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboUF.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.cboUF.ForeColor = System.Drawing.Color.Black;
            this.cboUF.FormattingEnabled = true;
            this.cboUF.Location = new System.Drawing.Point(35, 108);
            this.cboUF.Name = "cboUF";
            this.cboUF.Size = new System.Drawing.Size(121, 21);
            this.cboUF.TabIndex = 32;
            // 
            // txtCEP
            // 
            this.txtCEP.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCEP.ForeColor = System.Drawing.Color.Black;
            this.txtCEP.Location = new System.Drawing.Point(39, 26);
            this.txtCEP.Mask = "00000-000";
            this.txtCEP.Name = "txtCEP";
            this.txtCEP.Size = new System.Drawing.Size(87, 20);
            this.txtCEP.TabIndex = 31;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Segoe UI", 8F, System.Drawing.FontStyle.Bold);
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(173, 83);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(26, 13);
            this.label4.TabIndex = 37;
            this.label4.Text = "N° :";
            // 
            // lblUF
            // 
            this.lblUF.AutoSize = true;
            this.lblUF.Font = new System.Drawing.Font("Segoe UI", 8F, System.Drawing.FontStyle.Bold);
            this.lblUF.ForeColor = System.Drawing.Color.Black;
            this.lblUF.Location = new System.Drawing.Point(12, 111);
            this.lblUF.Name = "lblUF";
            this.lblUF.Size = new System.Drawing.Size(24, 13);
            this.lblUF.TabIndex = 36;
            this.lblUF.Text = "UF:";
            // 
            // txtComplemento
            // 
            this.txtComplemento.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.txtComplemento.ForeColor = System.Drawing.Color.Black;
            this.txtComplemento.Location = new System.Drawing.Point(96, 80);
            this.txtComplemento.MaxLength = 10000;
            this.txtComplemento.Name = "txtComplemento";
            this.txtComplemento.Size = new System.Drawing.Size(71, 20);
            this.txtComplemento.TabIndex = 33;
            // 
            // nudVR
            // 
            this.nudVR.Cursor = System.Windows.Forms.Cursors.Hand;
            this.nudVR.DecimalPlaces = 2;
            this.nudVR.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.nudVR.ForeColor = System.Drawing.Color.Black;
            this.nudVR.Increment = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.nudVR.Location = new System.Drawing.Point(248, 56);
            this.nudVR.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.nudVR.Name = "nudVR";
            this.nudVR.Size = new System.Drawing.Size(140, 20);
            this.nudVR.TabIndex = 40;
            this.nudVR.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblVR
            // 
            this.lblVR.AutoSize = true;
            this.lblVR.Font = new System.Drawing.Font("Segoe UI", 8F, System.Drawing.FontStyle.Bold);
            this.lblVR.ForeColor = System.Drawing.Color.Black;
            this.lblVR.Location = new System.Drawing.Point(226, 58);
            this.lblVR.Name = "lblVR";
            this.lblVR.Size = new System.Drawing.Size(24, 13);
            this.lblVR.TabIndex = 39;
            this.lblVR.Text = "VR:";
            // 
            // lblVA
            // 
            this.lblVA.AutoSize = true;
            this.lblVA.Font = new System.Drawing.Font("Segoe UI", 8F, System.Drawing.FontStyle.Bold);
            this.lblVA.ForeColor = System.Drawing.Color.Black;
            this.lblVA.Location = new System.Drawing.Point(12, 92);
            this.lblVA.Name = "lblVA";
            this.lblVA.Size = new System.Drawing.Size(25, 13);
            this.lblVA.TabIndex = 39;
            this.lblVA.Text = "VA:";
            // 
            // lblVT
            // 
            this.lblVT.AutoSize = true;
            this.lblVT.Font = new System.Drawing.Font("Segoe UI", 8F, System.Drawing.FontStyle.Bold);
            this.lblVT.ForeColor = System.Drawing.Color.Black;
            this.lblVT.Location = new System.Drawing.Point(12, 58);
            this.lblVT.Name = "lblVT";
            this.lblVT.Size = new System.Drawing.Size(23, 13);
            this.lblVT.TabIndex = 39;
            this.lblVT.Text = "VT:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 8F, System.Drawing.FontStyle.Bold);
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(12, 32);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(45, 13);
            this.label1.TabIndex = 39;
            this.label1.Text = "Salário:";
            // 
            // gpbAcesso
            // 
            this.gpbAcesso.Controls.Add(this.chkVendas);
            this.gpbAcesso.Controls.Add(this.txtSenha);
            this.gpbAcesso.Controls.Add(this.txtUsuario);
            this.gpbAcesso.Controls.Add(this.label3);
            this.gpbAcesso.Controls.Add(this.lblUsuario);
            this.gpbAcesso.Controls.Add(this.chkFinanceiro);
            this.gpbAcesso.Controls.Add(this.chkCompras);
            this.gpbAcesso.Controls.Add(this.chkRH);
            this.gpbAcesso.Controls.Add(this.chkFuncionario);
            this.gpbAcesso.Controls.Add(this.chkADM);
            this.gpbAcesso.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gpbAcesso.ForeColor = System.Drawing.Color.DarkGreen;
            this.gpbAcesso.Location = new System.Drawing.Point(415, 10);
            this.gpbAcesso.Name = "gpbAcesso";
            this.gpbAcesso.Size = new System.Drawing.Size(213, 418);
            this.gpbAcesso.TabIndex = 9;
            this.gpbAcesso.TabStop = false;
            this.gpbAcesso.Text = "Acesso ";
            this.gpbAcesso.Paint += new System.Windows.Forms.PaintEventHandler(this.gpbAcesso_Paint);
            // 
            // chkVendas
            // 
            this.chkVendas.AutoSize = true;
            this.chkVendas.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkVendas.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.chkVendas.ForeColor = System.Drawing.Color.Black;
            this.chkVendas.Location = new System.Drawing.Point(12, 368);
            this.chkVendas.Name = "chkVendas";
            this.chkVendas.Size = new System.Drawing.Size(76, 23);
            this.chkVendas.TabIndex = 36;
            this.chkVendas.Text = "Vendas";
            this.chkVendas.UseVisualStyleBackColor = true;
            // 
            // txtSenha
            // 
            this.txtSenha.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.txtSenha.ForeColor = System.Drawing.Color.Black;
            this.txtSenha.Location = new System.Drawing.Point(59, 73);
            this.txtSenha.Name = "txtSenha";
            this.txtSenha.Size = new System.Drawing.Size(147, 20);
            this.txtSenha.TabIndex = 35;
            this.txtSenha.UseSystemPasswordChar = true;
            // 
            // txtUsuario
            // 
            this.txtUsuario.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.txtUsuario.ForeColor = System.Drawing.Color.Black;
            this.txtUsuario.Location = new System.Drawing.Point(69, 44);
            this.txtUsuario.Name = "txtUsuario";
            this.txtUsuario.Size = new System.Drawing.Size(138, 20);
            this.txtUsuario.TabIndex = 34;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(8, 74);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 19);
            this.label3.TabIndex = 2;
            this.label3.Text = "Senha:";
            // 
            // lblUsuario
            // 
            this.lblUsuario.AutoSize = true;
            this.lblUsuario.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.lblUsuario.ForeColor = System.Drawing.Color.Black;
            this.lblUsuario.Location = new System.Drawing.Point(8, 44);
            this.lblUsuario.Name = "lblUsuario";
            this.lblUsuario.Size = new System.Drawing.Size(64, 19);
            this.lblUsuario.TabIndex = 1;
            this.lblUsuario.Text = "Usuário:";
            // 
            // chkFinanceiro
            // 
            this.chkFinanceiro.AutoSize = true;
            this.chkFinanceiro.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkFinanceiro.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.chkFinanceiro.ForeColor = System.Drawing.Color.Black;
            this.chkFinanceiro.Location = new System.Drawing.Point(12, 338);
            this.chkFinanceiro.Name = "chkFinanceiro";
            this.chkFinanceiro.Size = new System.Drawing.Size(97, 23);
            this.chkFinanceiro.TabIndex = 0;
            this.chkFinanceiro.Text = "Financeiro";
            this.chkFinanceiro.UseVisualStyleBackColor = true;
            // 
            // chkCompras
            // 
            this.chkCompras.AutoSize = true;
            this.chkCompras.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkCompras.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.chkCompras.ForeColor = System.Drawing.Color.Black;
            this.chkCompras.Location = new System.Drawing.Point(12, 258);
            this.chkCompras.Name = "chkCompras";
            this.chkCompras.Size = new System.Drawing.Size(88, 23);
            this.chkCompras.TabIndex = 0;
            this.chkCompras.Text = "Compras";
            this.chkCompras.UseVisualStyleBackColor = true;
            // 
            // chkRH
            // 
            this.chkRH.AutoSize = true;
            this.chkRH.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkRH.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.chkRH.ForeColor = System.Drawing.Color.Black;
            this.chkRH.Location = new System.Drawing.Point(12, 228);
            this.chkRH.Name = "chkRH";
            this.chkRH.Size = new System.Drawing.Size(48, 23);
            this.chkRH.TabIndex = 0;
            this.chkRH.Text = "RH";
            this.chkRH.UseVisualStyleBackColor = true;
            // 
            // chkFuncionario
            // 
            this.chkFuncionario.AutoSize = true;
            this.chkFuncionario.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkFuncionario.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.chkFuncionario.ForeColor = System.Drawing.Color.Black;
            this.chkFuncionario.Location = new System.Drawing.Point(12, 163);
            this.chkFuncionario.Name = "chkFuncionario";
            this.chkFuncionario.Size = new System.Drawing.Size(106, 23);
            this.chkFuncionario.TabIndex = 0;
            this.chkFuncionario.Text = "Funcionário";
            this.chkFuncionario.UseVisualStyleBackColor = true;
            // 
            // chkADM
            // 
            this.chkADM.AutoSize = true;
            this.chkADM.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkADM.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.chkADM.ForeColor = System.Drawing.Color.Black;
            this.chkADM.Location = new System.Drawing.Point(12, 132);
            this.chkADM.Name = "chkADM";
            this.chkADM.Size = new System.Drawing.Size(61, 23);
            this.chkADM.TabIndex = 0;
            this.chkADM.Text = "ADM";
            this.chkADM.UseVisualStyleBackColor = true;
            // 
            // nudSalario
            // 
            this.nudSalario.Cursor = System.Windows.Forms.Cursors.Hand;
            this.nudSalario.DecimalPlaces = 2;
            this.nudSalario.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.nudSalario.ForeColor = System.Drawing.Color.Black;
            this.nudSalario.Increment = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.nudSalario.Location = new System.Drawing.Point(56, 30);
            this.nudSalario.Name = "nudSalario";
            this.nudSalario.Size = new System.Drawing.Size(140, 20);
            this.nudSalario.TabIndex = 40;
            this.nudSalario.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.Maroon;
            this.button2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.button2.ForeColor = System.Drawing.Color.White;
            this.button2.Location = new System.Drawing.Point(474, 434);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 28);
            this.button2.TabIndex = 11;
            this.button2.Text = "Voltar";
            this.button2.UseVisualStyleBackColor = false;
            // 
            // lblCompemento
            // 
            this.lblCompemento.AutoSize = true;
            this.lblCompemento.Font = new System.Drawing.Font("Segoe UI", 8F, System.Drawing.FontStyle.Bold);
            this.lblCompemento.ForeColor = System.Drawing.Color.Black;
            this.lblCompemento.Location = new System.Drawing.Point(12, 82);
            this.lblCompemento.Name = "lblCompemento";
            this.lblCompemento.Size = new System.Drawing.Size(84, 13);
            this.lblCompemento.TabIndex = 35;
            this.lblCompemento.Text = "Complemento:";
            // 
            // gpbDadosPessoais
            // 
            this.gpbDadosPessoais.Controls.Add(this.txtTelefone);
            this.gpbDadosPessoais.Controls.Add(this.lblTelefone);
            this.gpbDadosPessoais.Controls.Add(this.btnProcurar);
            this.gpbDadosPessoais.Controls.Add(this.pictureBox1);
            this.gpbDadosPessoais.Controls.Add(this.dtpNascimento);
            this.gpbDadosPessoais.Controls.Add(this.txtRG);
            this.gpbDadosPessoais.Controls.Add(this.chkM);
            this.gpbDadosPessoais.Controls.Add(this.chkF);
            this.gpbDadosPessoais.Controls.Add(this.txtCPF);
            this.gpbDadosPessoais.Controls.Add(this.txtNome);
            this.gpbDadosPessoais.Controls.Add(this.lblSexo);
            this.gpbDadosPessoais.Controls.Add(this.lblDataNascimento);
            this.gpbDadosPessoais.Controls.Add(this.lblRG);
            this.gpbDadosPessoais.Controls.Add(this.lblCPF);
            this.gpbDadosPessoais.Controls.Add(this.lblNome);
            this.gpbDadosPessoais.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gpbDadosPessoais.ForeColor = System.Drawing.Color.DarkGreen;
            this.gpbDadosPessoais.Location = new System.Drawing.Point(11, 10);
            this.gpbDadosPessoais.Name = "gpbDadosPessoais";
            this.gpbDadosPessoais.Size = new System.Drawing.Size(398, 145);
            this.gpbDadosPessoais.TabIndex = 6;
            this.gpbDadosPessoais.TabStop = false;
            this.gpbDadosPessoais.Text = "Dados Pessoais ";
            this.gpbDadosPessoais.Paint += new System.Windows.Forms.PaintEventHandler(this.gpbDadosPessoais_Paint);
            // 
            // txtTelefone
            // 
            this.txtTelefone.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.txtTelefone.ForeColor = System.Drawing.Color.Black;
            this.txtTelefone.Location = new System.Drawing.Point(68, 118);
            this.txtTelefone.Mask = "(99) 99999-9999";
            this.txtTelefone.Name = "txtTelefone";
            this.txtTelefone.Size = new System.Drawing.Size(96, 20);
            this.txtTelefone.TabIndex = 28;
            // 
            // lblTelefone
            // 
            this.lblTelefone.AutoSize = true;
            this.lblTelefone.Font = new System.Drawing.Font("Segoe UI", 8F, System.Drawing.FontStyle.Bold);
            this.lblTelefone.ForeColor = System.Drawing.Color.Black;
            this.lblTelefone.Location = new System.Drawing.Point(11, 121);
            this.lblTelefone.Name = "lblTelefone";
            this.lblTelefone.Size = new System.Drawing.Size(55, 13);
            this.lblTelefone.TabIndex = 29;
            this.lblTelefone.Text = "Telefone:";
            // 
            // btnProcurar
            // 
            this.btnProcurar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnProcurar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnProcurar.Font = new System.Drawing.Font("Segoe UI", 8F, System.Drawing.FontStyle.Bold);
            this.btnProcurar.ForeColor = System.Drawing.Color.Black;
            this.btnProcurar.Location = new System.Drawing.Point(300, 105);
            this.btnProcurar.Name = "btnProcurar";
            this.btnProcurar.Size = new System.Drawing.Size(84, 23);
            this.btnProcurar.TabIndex = 13;
            this.btnProcurar.Text = "Procurar";
            this.btnProcurar.UseVisualStyleBackColor = true;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Nsf._2018.ProjetoIntegrador.PuroTempero.Properties.Resources.noun_people_66664;
            this.pictureBox1.Location = new System.Drawing.Point(300, 17);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(84, 78);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 12;
            this.pictureBox1.TabStop = false;
            // 
            // dtpNascimento
            // 
            this.dtpNascimento.CalendarFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpNascimento.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.dtpNascimento.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpNascimento.Location = new System.Drawing.Point(125, 93);
            this.dtpNascimento.Name = "dtpNascimento";
            this.dtpNascimento.Size = new System.Drawing.Size(142, 20);
            this.dtpNascimento.TabIndex = 11;
            // 
            // txtRG
            // 
            this.txtRG.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRG.ForeColor = System.Drawing.Color.Black;
            this.txtRG.Location = new System.Drawing.Point(190, 51);
            this.txtRG.Mask = "99,999,999-9";
            this.txtRG.Name = "txtRG";
            this.txtRG.Size = new System.Drawing.Size(75, 20);
            this.txtRG.TabIndex = 8;
            // 
            // chkM
            // 
            this.chkM.AutoSize = true;
            this.chkM.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkM.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkM.ForeColor = System.Drawing.Color.Black;
            this.chkM.Location = new System.Drawing.Point(85, 75);
            this.chkM.Name = "chkM";
            this.chkM.Size = new System.Drawing.Size(34, 17);
            this.chkM.TabIndex = 10;
            this.chkM.TabStop = true;
            this.chkM.Text = "M";
            this.chkM.UseVisualStyleBackColor = true;
            // 
            // chkF
            // 
            this.chkF.AutoSize = true;
            this.chkF.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkF.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkF.ForeColor = System.Drawing.Color.Black;
            this.chkF.Location = new System.Drawing.Point(48, 75);
            this.chkF.Name = "chkF";
            this.chkF.Size = new System.Drawing.Size(31, 17);
            this.chkF.TabIndex = 9;
            this.chkF.TabStop = true;
            this.chkF.Text = "F";
            this.chkF.UseVisualStyleBackColor = true;
            // 
            // txtCPF
            // 
            this.txtCPF.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCPF.ForeColor = System.Drawing.Color.Black;
            this.txtCPF.Location = new System.Drawing.Point(48, 51);
            this.txtCPF.Mask = "00,000,000/0000-00";
            this.txtCPF.Name = "txtCPF";
            this.txtCPF.Size = new System.Drawing.Size(108, 20);
            this.txtCPF.TabIndex = 7;
            // 
            // txtNome
            // 
            this.txtNome.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNome.ForeColor = System.Drawing.Color.Black;
            this.txtNome.Location = new System.Drawing.Point(52, 26);
            this.txtNome.MaxLength = 50;
            this.txtNome.Name = "txtNome";
            this.txtNome.Size = new System.Drawing.Size(213, 20);
            this.txtNome.TabIndex = 6;
            // 
            // lblSexo
            // 
            this.lblSexo.AutoSize = true;
            this.lblSexo.Font = new System.Drawing.Font("Segoe UI", 8F, System.Drawing.FontStyle.Bold);
            this.lblSexo.ForeColor = System.Drawing.Color.Black;
            this.lblSexo.Location = new System.Drawing.Point(12, 77);
            this.lblSexo.Name = "lblSexo";
            this.lblSexo.Size = new System.Drawing.Size(35, 13);
            this.lblSexo.TabIndex = 4;
            this.lblSexo.Text = "Sexo:";
            // 
            // lblDataNascimento
            // 
            this.lblDataNascimento.AutoSize = true;
            this.lblDataNascimento.Font = new System.Drawing.Font("Segoe UI", 8F, System.Drawing.FontStyle.Bold);
            this.lblDataNascimento.ForeColor = System.Drawing.Color.Black;
            this.lblDataNascimento.Location = new System.Drawing.Point(12, 99);
            this.lblDataNascimento.Name = "lblDataNascimento";
            this.lblDataNascimento.Size = new System.Drawing.Size(115, 13);
            this.lblDataNascimento.TabIndex = 1;
            this.lblDataNascimento.Text = "Data de Nascimento:";
            // 
            // lblRG
            // 
            this.lblRG.AutoSize = true;
            this.lblRG.Font = new System.Drawing.Font("Segoe UI", 8F, System.Drawing.FontStyle.Bold);
            this.lblRG.ForeColor = System.Drawing.Color.Black;
            this.lblRG.Location = new System.Drawing.Point(166, 54);
            this.lblRG.Name = "lblRG";
            this.lblRG.Size = new System.Drawing.Size(25, 13);
            this.lblRG.TabIndex = 1;
            this.lblRG.Text = "RG:";
            // 
            // lblCPF
            // 
            this.lblCPF.AutoSize = true;
            this.lblCPF.Font = new System.Drawing.Font("Segoe UI", 8F, System.Drawing.FontStyle.Bold);
            this.lblCPF.ForeColor = System.Drawing.Color.Black;
            this.lblCPF.Location = new System.Drawing.Point(12, 54);
            this.lblCPF.Name = "lblCPF";
            this.lblCPF.Size = new System.Drawing.Size(30, 13);
            this.lblCPF.TabIndex = 1;
            this.lblCPF.Text = "CPF:";
            // 
            // lblNome
            // 
            this.lblNome.AutoSize = true;
            this.lblNome.Font = new System.Drawing.Font("Segoe UI", 8F, System.Drawing.FontStyle.Bold);
            this.lblNome.ForeColor = System.Drawing.Color.Black;
            this.lblNome.Location = new System.Drawing.Point(12, 29);
            this.lblNome.Name = "lblNome";
            this.lblNome.Size = new System.Drawing.Size(42, 13);
            this.lblNome.TabIndex = 0;
            this.lblNome.Text = "Nome:";
            // 
            // btnAlterar
            // 
            this.btnAlterar.BackColor = System.Drawing.Color.DarkGreen;
            this.btnAlterar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnAlterar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAlterar.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.btnAlterar.ForeColor = System.Drawing.Color.White;
            this.btnAlterar.Location = new System.Drawing.Point(553, 434);
            this.btnAlterar.Name = "btnAlterar";
            this.btnAlterar.Size = new System.Drawing.Size(75, 28);
            this.btnAlterar.TabIndex = 10;
            this.btnAlterar.Text = "Alterar";
            this.btnAlterar.UseVisualStyleBackColor = false;
            // 
            // gpbEndereco
            // 
            this.gpbEndereco.Controls.Add(this.cboCidade);
            this.gpbEndereco.Controls.Add(this.lblCidade);
            this.gpbEndereco.Controls.Add(this.imagePesquisa);
            this.gpbEndereco.Controls.Add(this.nudNumero);
            this.gpbEndereco.Controls.Add(this.cboUF);
            this.gpbEndereco.Controls.Add(this.txtCEP);
            this.gpbEndereco.Controls.Add(this.label4);
            this.gpbEndereco.Controls.Add(this.lblUF);
            this.gpbEndereco.Controls.Add(this.txtComplemento);
            this.gpbEndereco.Controls.Add(this.lblCompemento);
            this.gpbEndereco.Controls.Add(this.lblEndereco);
            this.gpbEndereco.Controls.Add(this.txtEndereco);
            this.gpbEndereco.Controls.Add(this.lblCEP);
            this.gpbEndereco.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gpbEndereco.ForeColor = System.Drawing.Color.DarkGreen;
            this.gpbEndereco.Location = new System.Drawing.Point(11, 161);
            this.gpbEndereco.Name = "gpbEndereco";
            this.gpbEndereco.Size = new System.Drawing.Size(398, 140);
            this.gpbEndereco.TabIndex = 7;
            this.gpbEndereco.TabStop = false;
            this.gpbEndereco.Text = "Endereço";
            this.gpbEndereco.Paint += new System.Windows.Forms.PaintEventHandler(this.gpbEndereco_Paint);
            // 
            // imagePesquisa
            // 
            this.imagePesquisa.Cursor = System.Windows.Forms.Cursors.Hand;
            this.imagePesquisa.Image = global::Nsf._2018.ProjetoIntegrador.PuroTempero.Properties.Resources.noun_Search_755263;
            this.imagePesquisa.Location = new System.Drawing.Point(126, 23);
            this.imagePesquisa.Name = "imagePesquisa";
            this.imagePesquisa.Size = new System.Drawing.Size(23, 23);
            this.imagePesquisa.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imagePesquisa.TabIndex = 39;
            this.imagePesquisa.TabStop = false;
            // 
            // lblEndereco
            // 
            this.lblEndereco.AutoSize = true;
            this.lblEndereco.Font = new System.Drawing.Font("Segoe UI", 8F, System.Drawing.FontStyle.Bold);
            this.lblEndereco.ForeColor = System.Drawing.Color.Black;
            this.lblEndereco.Location = new System.Drawing.Point(11, 55);
            this.lblEndereco.Name = "lblEndereco";
            this.lblEndereco.Size = new System.Drawing.Size(58, 13);
            this.lblEndereco.TabIndex = 28;
            this.lblEndereco.Text = "Endereço:";
            // 
            // txtEndereco
            // 
            this.txtEndereco.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEndereco.ForeColor = System.Drawing.Color.Black;
            this.txtEndereco.Location = new System.Drawing.Point(67, 52);
            this.txtEndereco.MaxLength = 70;
            this.txtEndereco.Name = "txtEndereco";
            this.txtEndereco.Size = new System.Drawing.Size(317, 20);
            this.txtEndereco.TabIndex = 29;
            // 
            // lblCEP
            // 
            this.lblCEP.AutoSize = true;
            this.lblCEP.Font = new System.Drawing.Font("Segoe UI", 8F, System.Drawing.FontStyle.Bold);
            this.lblCEP.ForeColor = System.Drawing.Color.Black;
            this.lblCEP.Location = new System.Drawing.Point(11, 28);
            this.lblCEP.Name = "lblCEP";
            this.lblCEP.Size = new System.Drawing.Size(30, 13);
            this.lblCEP.TabIndex = 30;
            this.lblCEP.Text = "CEP:";
            // 
            // gpbSalario
            // 
            this.gpbSalario.Controls.Add(this.nudConvenio);
            this.gpbSalario.Controls.Add(this.nudVR);
            this.gpbSalario.Controls.Add(this.nudVA);
            this.gpbSalario.Controls.Add(this.lblConvenio);
            this.gpbSalario.Controls.Add(this.nudVT);
            this.gpbSalario.Controls.Add(this.lblVR);
            this.gpbSalario.Controls.Add(this.lblVA);
            this.gpbSalario.Controls.Add(this.nudSalario);
            this.gpbSalario.Controls.Add(this.lblVT);
            this.gpbSalario.Controls.Add(this.label1);
            this.gpbSalario.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gpbSalario.ForeColor = System.Drawing.Color.DarkGreen;
            this.gpbSalario.Location = new System.Drawing.Point(11, 307);
            this.gpbSalario.Name = "gpbSalario";
            this.gpbSalario.Size = new System.Drawing.Size(398, 121);
            this.gpbSalario.TabIndex = 8;
            this.gpbSalario.TabStop = false;
            this.gpbSalario.Text = "Base Salarial";
            this.gpbSalario.Paint += new System.Windows.Forms.PaintEventHandler(this.gpbSalario_Paint);
            // 
            // frmAlterar_Funcionario
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gpbAcesso);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.gpbDadosPessoais);
            this.Controls.Add(this.btnAlterar);
            this.Controls.Add(this.gpbEndereco);
            this.Controls.Add(this.gpbSalario);
            this.Name = "frmAlterar_Funcionario";
            this.Size = new System.Drawing.Size(639, 473);
            ((System.ComponentModel.ISupportInitialize)(this.nudConvenio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudVA)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudVT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudNumero)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudVR)).EndInit();
            this.gpbAcesso.ResumeLayout(false);
            this.gpbAcesso.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudSalario)).EndInit();
            this.gpbDadosPessoais.ResumeLayout(false);
            this.gpbDadosPessoais.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.gpbEndereco.ResumeLayout(false);
            this.gpbEndereco.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imagePesquisa)).EndInit();
            this.gpbSalario.ResumeLayout(false);
            this.gpbSalario.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.NumericUpDown nudConvenio;
        private System.Windows.Forms.NumericUpDown nudVA;
        private System.Windows.Forms.Label lblConvenio;
        private System.Windows.Forms.NumericUpDown nudVT;
        private System.Windows.Forms.ComboBox cboCidade;
        private System.Windows.Forms.Label lblCidade;
        private System.Windows.Forms.PictureBox imagePesquisa;
        private System.Windows.Forms.NumericUpDown nudNumero;
        private System.Windows.Forms.ComboBox cboUF;
        private System.Windows.Forms.MaskedTextBox txtCEP;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lblUF;
        private System.Windows.Forms.TextBox txtComplemento;
        private System.Windows.Forms.NumericUpDown nudVR;
        private System.Windows.Forms.Label lblVR;
        private System.Windows.Forms.Label lblVA;
        private System.Windows.Forms.Label lblVT;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox gpbAcesso;
        private System.Windows.Forms.CheckBox chkVendas;
        private System.Windows.Forms.TextBox txtSenha;
        private System.Windows.Forms.TextBox txtUsuario;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblUsuario;
        private System.Windows.Forms.CheckBox chkFinanceiro;
        private System.Windows.Forms.CheckBox chkCompras;
        private System.Windows.Forms.CheckBox chkRH;
        private System.Windows.Forms.CheckBox chkFuncionario;
        private System.Windows.Forms.CheckBox chkADM;
        private System.Windows.Forms.NumericUpDown nudSalario;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label lblCompemento;
        private System.Windows.Forms.GroupBox gpbDadosPessoais;
        private System.Windows.Forms.MaskedTextBox txtTelefone;
        private System.Windows.Forms.Label lblTelefone;
        private System.Windows.Forms.Button btnProcurar;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.DateTimePicker dtpNascimento;
        private System.Windows.Forms.MaskedTextBox txtRG;
        private System.Windows.Forms.RadioButton chkM;
        private System.Windows.Forms.RadioButton chkF;
        private System.Windows.Forms.MaskedTextBox txtCPF;
        private System.Windows.Forms.TextBox txtNome;
        private System.Windows.Forms.Label lblSexo;
        private System.Windows.Forms.Label lblDataNascimento;
        private System.Windows.Forms.Label lblRG;
        private System.Windows.Forms.Label lblCPF;
        private System.Windows.Forms.Label lblNome;
        private System.Windows.Forms.Button btnAlterar;
        private System.Windows.Forms.GroupBox gpbEndereco;
        private System.Windows.Forms.Label lblEndereco;
        private System.Windows.Forms.TextBox txtEndereco;
        private System.Windows.Forms.Label lblCEP;
        private System.Windows.Forms.GroupBox gpbSalario;
    }
}
